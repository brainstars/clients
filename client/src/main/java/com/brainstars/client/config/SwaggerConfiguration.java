package com.brainstars.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
@Import({SpringDataRestConfiguration.class, BeanValidatorPluginsConfiguration.class})
public class SwaggerConfiguration {

    @Bean
    public Docket clientApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/rest-api/clients/**"))
                .apis(RequestHandlerSelectors.basePackage("com.brainstars.client.controller"))
                .build()
                .apiInfo(clientApiInfo());
    }

    private ApiInfo clientApiInfo() {
        return new ApiInfo(
                "Clients Rest API",
                "Clients Rest API that works with clients.",
                "1.0.0",
                "Link to the page that describes the terms of service.",
                new Contact("Nikolay Uzunov", "", "nikolay.uzunov@brainstarstech.com"),
                "License of API", "API license URL", Collections.emptyList());
    }
}
