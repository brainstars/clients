package com.brainstars.client.controller;

import com.brainstars.client.model.ClientModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/rest-api/clients")
public class ClientController {

    @ApiOperation(value = "View a list of clients.", response = List.class)
    @GetMapping
    public List<ClientModel> getAll() {
        return Arrays.asList(new ClientModel(1L, "ClientName"), new ClientModel(2L, "ClientName2"));
    }
}
