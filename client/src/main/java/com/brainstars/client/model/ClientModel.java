package com.brainstars.client.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Details about Client.")
public class ClientModel {

    @ApiModelProperty("Client's Identifier.")
    Long id;

    @ApiModelProperty("Client's name.")
    String name;
}


